import React from "react";
import { Image } from "react-bootstrap";
import "./Maintenance.css";

const MaintenancePhoto = require("../../assets/images/maintenance.png");

const Maintenance = () => {
  return (
    <>
      <Image src={MaintenancePhoto} style={{ width: "100%" }} />
    </>
  );
};

export default Maintenance;
