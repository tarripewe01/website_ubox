import Home from "./home/Home";
import About from "./about/About";
import Produk from "./produk/Produk";
import Bisnis from "./bisnis/Bisnis";
import Maintenance from "./maintenance/Maintenance";

export { Home, About, Produk, Bisnis, Maintenance };
