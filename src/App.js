import React from 'react';
import { Route, BrowserRouter as Router, Routes } from 'react-router-dom';
import { Header } from "./components";
import { Home, Maintenance } from "./pages";


function App() {
  return (
    <Router>
    <Header />
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/about" element={<Maintenance />} />
      <Route path="/produk" element={<Maintenance />} />
      <Route path="/bisnis" element={<Maintenance />} />
    </Routes>
  </Router>
  );
}

export default App;
