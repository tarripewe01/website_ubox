import React from "react";
import { Container, Image } from "react-bootstrap";
import { Link } from "react-router-dom";
import "./BannerHome.css";

const Playstore = require("../../../assets/images/playstore.png");
const Appstore = require("../../../assets/images/appstore.png");

const BannerHome = () => {
  return (
    <div
      className="background-container"
      style={{
        backgroundSize: "cover",
        backgroundPosition: "center",
        minHeight: "100vh",
        display: "flex",
        alignItems: "center",
      }}
    >
      <Container>
        <h1
          style={{
            color: "white",
            fontSize: "45px",
            textAlign: "left",
            maxWidth: "600px",
            marginTop: -130,
          }}
        >
          Daya Tanpa Batas Untuk Gadget Kamu !
        </h1>
        <p
          style={{
            color: "white",
            maxWidth: "600px",
            textAlign: "justify",
            marginTop: 50,
          }}
        >
          Ubox adalah layanan sewa powerbank yang dirancang khusus untuk
          memenuhi kebutuhan mobilitas dan konektivitas modern. Kami
          menghadirkan powerbank berkualitas tinggi dengan kapasitas baterai
          yang besar, sehingga Anda tidak perlu khawatir tentang kehabisan daya
          saat bepergian, bekerja, atau bersantai.
        </p>
        <p
          style={{
            color: "white",
            maxWidth: "600px",
            textAlign: "justify",
            marginTop: 50,
          }}
        >
          Unduh aplikasinya dan temukan stasiun UBOX terdekat !
        </p>
        <Link
          to="https://play.google.com/store/apps/details?id=com.nusantech.ubox"
          target="_blank"
        >
          <Image src={Playstore} style={{ width: 200, cursor: "pointer" }} />
        </Link>
        <Link
          to="https://apps.apple.com/id/app/ubox/id6444171407?l=id"
          target="_blank"
        >
          <Image
            src={Appstore}
            style={{ width: 190, borderRadius: 10, cursor: "pointer" }}
          />
        </Link>
      </Container>
    </div>
  );
};

export default BannerHome;
